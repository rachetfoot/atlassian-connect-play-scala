package com.atlassian.connect.playscala.util.license

import com.atlassian.connect.playscala.AC.licenseCheck

import play.api.mvc.RequestHeader

/**
 * Atlassian connect license checker checking whether an active license is in use by checking the
 * request.
 *
 * IMPORTANT: License checking is disabled by default and should be enabled in Play configuration.
 *
 * Each incoming request from the Atlassian Host includes a query parameter named `lic`. The `lic`
 * parameter having a value of `active` determines that the license is valid.
 *
 * Note that this license checker works independent of Atlassian Host Key (tenant independent) so make sure to do
 * the JWT authorization independent of this checking.
 *
 * @author Nader Hadji Ghanbari
 *
 * @see https://developer.atlassian.com/static/connect/docs/latest/concepts/licensing.html#license-status
 */
object RequestLicenseChecker {

  /**
   * Checks whether an active license is in use by checking the `lic` query parameter value if
   * license checking is enabled in Play configuration.
   *
   * @param req Request.
   * @return Boolean determining whether license checking is disabled or an active license is in use.
   */
  def licenseIsActive()(implicit req: RequestHeader): Boolean = {
    (licenseCheck, req.getQueryString("lic")) match {
      case (false, _) => true
      case (true, Some("active")) => true
      case _ => false
    }
  }
}
