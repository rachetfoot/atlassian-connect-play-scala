package com.atlassian.connect.playscala.util

import com.atlassian.connect.playscala.auth.Token
import com.atlassian.connect.playscala.model.AcHostModel

import java.util.concurrent.TimeUnit
import java.net.URI

import play.api.Logger
import play.api.mvc.RequestHeader
import play.api.libs.ws.{ WSClient, WSRequestHolder, WS }

/**
 * Trait making it possible to send requests back to host application. Simply start your calls with
 * [[com.atlassian.connect.playscala.util.AcHostWS#uri(relativePath:String):WSRequestHolder*]].
 *
 * @since v1.0
 */
trait AcHostWS {

  def ws: WSClient

  val DefaultTimeout = TimeUnit.SECONDS.convert(5, TimeUnit.MILLISECONDS).toInt

  def uri(relativePath: String)(implicit token: Token): WSRequestHolder = uri(URI.create(relativePath))

  def uri(relativePathUri: URI)(implicit token: Token): WSRequestHolder = uri(token.acHost, relativePathUri)

  def uri(acHost: AcHostModel, relativePath: String): WSRequestHolder = uri(acHost, URI.create(relativePath))

  def uri(acHost: AcHostModel, relativePathUri: URI): WSRequestHolder = {
    val absoluteUrl = buildAbsoluteUrl(URI.create(acHost.baseUrl), relativePathUri)
    Logger.debug(s"Url: ${absoluteUrl.toString}")
    ws.url(absoluteUrl.toString).withRequestTimeout(DefaultTimeout)
  }

  private def buildAbsoluteUrl(baseUrl: URI, path: URI): URI = {
    if (path.isAbsolute) throw new IllegalArgumentException("must not be absolute") else URI.create(baseUrl.toString + "/" + path.toString).normalize()
  }

}

/**
 * Singleton AcHostWS using Play WS default client.
 */
object AcHostWS extends AcHostWS {

  import play.api.Play.current

  override def ws = WS.client

}
