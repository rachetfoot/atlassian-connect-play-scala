package com.atlassian.connect.playscala.store

import play.api.Application
import com.atlassian.connect.playscala.model.AcHostModelStore

trait DbConfiguration {
  def onStart(app: Application) {}
  def acHostModelStore: AcHostModelStore
}
