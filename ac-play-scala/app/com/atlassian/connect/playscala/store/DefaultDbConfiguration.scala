package com.atlassian.connect.playscala.store

import com.atlassian.connect.playscala.model.AcHostModelStore
import play.api.{ Application, Configuration }
import com.atlassian.connect.playscala.store.slick.SlickDbConfiguration

trait DefaultDbConfiguration extends DbConfiguration {
  override def onStart(app: Application): Unit = DefaultDbConfiguration.delegate.onStart(app)
  def acHostModelStore: AcHostModelStore = DefaultDbConfiguration.delegate.acHostModelStore
}

object DefaultDbConfiguration {
  val classLoader = this.getClass.getClassLoader
  lazy val delegate: DbConfiguration = loadSingletonObject[DbConfiguration](play.api.Play.current.configuration, SlickDbConfiguration)

  def loadSingletonObject[A](configuration: Configuration, defaultObject: => A)(implicit manifest: Manifest[A]): A = {
    val readProviderName: Option[String] = configuration.getString("provider." + manifest.runtimeClass.getName)
    readProviderName.map(clazzName => classLoader.loadClass(clazzName + "$").getField("MODULE$").get(manifest.runtimeClass).asInstanceOf[A]).getOrElse(defaultObject)
  }

  def onStart(app: Application): Unit = delegate.onStart(app)
}
