package com.atlassian.connect.playscala.store.slick

import scala.slick.driver.JdbcProfile
import com.atlassian.connect.playscala.model.{ AcHostModel, ClientKey }

trait SlickDbSchema {

  val profile: JdbcProfile
  import profile.simple._

  implicit val ClientKeyMapper = MappedColumnType.base[ClientKey, String](_.value, ClientKey)

  class AcHostModelSchema(tag: Tag) extends Table[AcHostModel](tag, "ac_host") {
    def id = column[Option[Long]]("id", O.PrimaryKey, O.AutoInc)
    def key = column[ClientKey]("key", O.NotNull)
    def keyIdx = index("uq_ac_host_key", key, unique = true)
    def publicKey = column[String]("publickey", O.NotNull, O.DBType("VARCHAR(512)"))
    def baseUrl = column[String]("baseurl", O.NotNull, O.DBType("VARCHAR(512)"))
    def baseUrlIdx = index("uq_ac_host_base_url", baseUrl, unique = true)
    def name = column[String]("name")
    def description = column[Option[String]]("description")
    def sharedSecret = column[String]("sharedsecret")
    def oauthClientId = column[Option[String]]("oauthclientid")

    def * = (id, key, publicKey, baseUrl, name, description, sharedSecret, oauthClientId) <> (AcHostModel.tupled, AcHostModel.unapply)
  }

  val acHostModelSchema = TableQuery[AcHostModelSchema]
}
