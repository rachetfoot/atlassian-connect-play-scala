package com.atlassian.connect.playscala

object Constants {
  val AC_TOKEN_EXPIRY = "ac.token.expiry.secs"
  val AC_DEV = "ac.dev"
  val AC_PLUGIN_KEY = "ac.key"
  val AC_PLUGIN_NAME = "ac.name"
  val AC_PLUGIN_VERSION = "ac.version"
  val AC_PLUGIN_LICENSE_CHECK = "ac.license.check"

  // must not use '.' in the config key if environment variable lookup is needed, as it will cause typesafe config to complain:
  // com.typesafe.config.ConfigException$WrongType: env var OAUTH_LOCAL_PUBLIC_KEY: ac.oauth.local.public_key has type STRING rather than OBJECT
  val AC_OAUTH_LOCAL_PUBLIC_KEY_FILE = "ac_oauth_local_public_key_file"
  val AC_OAUTH_LOCAL_PRIVATE_KEY_FILE = "ac_oauth_local_private_key_file"
  val AC_OAUTH_LOCAL_PUBLIC_KEY = "ac_oauth_local_public_key"
  val AC_OAUTH_LOCAL_PRIVATE_KEY = "ac_oauth_local_private_key"

  val AC_ACCOUNT_ID_PARAM = "account_id"
  val AC_HOST_PARAM = "ac_host"
  val AC_TOKEN = "ac_token"

  val AUTHORIZATION_BASE_URL = "https://auth.atlassian.io"
  val AUTHORIZATION_PATH = "/oauth2/token"
  val JWT_BEARER_URN = "urn:ietf:params:oauth:grant-type:jwt-bearer"
}
