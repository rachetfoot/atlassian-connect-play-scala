package com.atlassian.connect.playscala.auth.jwt

import java.util.concurrent.TimeUnit

import com.atlassian.connect.playscala.AcConfigured
import com.atlassian.connect.playscala.auth.ActingAsUserToken
import com.atlassian.connect.playscala.Constants.{ AUTHORIZATION_BASE_URL, AUTHORIZATION_PATH, JWT_BEARER_URN }
import com.atlassian.jwt.SigningAlgorithm
import com.atlassian.jwt.core.writer.NimbusJwtWriterFactory
import spray.caching._
import play.api.Logger
import play.api.libs.json.Json
import play.api.libs.ws.{ WS, WSResponse }
import play.api.Play.current

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

trait AcBearerTokenGenerator extends JwtConfig with AcConfigured {
  private val jwtWriterFactory = new NimbusJwtWriterFactory()
  private val TOKEN_EXPIRY_MARGIN = Duration(10, TimeUnit.SECONDS)
  private val bearerTokenCache: Cache[AccessToken] = LruCache(1500, 100, Duration(15, TimeUnit.MINUTES))

  private def cacheKey(implicit token: ActingAsUserToken) = token.acHost.key.value + token.userAccountId

  case class AccessToken(value: String, expiresAt: Deadline)
  case class RateLimitExceeded(message: String = "Atlasian authentication server rate limit exceeded",
    limitResetAt: Deadline) extends Exception

  def getAccessTokenForUser(implicit token: ActingAsUserToken): Future[AccessToken] = {
    Logger.debug("Checking for cached value for access token for 'ACT_AS_USER' scope")

    bearerTokenCache(cacheKey, () =>
      createAccessTokenRequest.execute map checkResponse map { response =>
        Logger.debug("Received access token from auth server")

        val json = Json.parse(response.body)
        (for {
          accessToken <- (json \ "access_token").asOpt[String]
          expiresIn <- (json \ "expires_in").asOpt[Long]
        } yield {
          AccessToken(accessToken, Deadline.now + Duration(expiresIn, TimeUnit.SECONDS))
        }) getOrElse (throw new Exception("Could not parse server response when retrieving bearer token"))
      }
    ) flatMap {
      case AccessToken(_, expiresAt) if (expiresAt + TOKEN_EXPIRY_MARGIN).isOverdue() =>
        Logger.debug("Cached access token has expired, let's get a new one")
        bearerTokenCache.remove(cacheKey)
        getAccessTokenForUser
      case accessToken: AccessToken =>
        Future.successful(accessToken)
    }
  }

  private def createAccessTokenRequest(implicit token: ActingAsUserToken) = {
    Logger.debug(s"Requesting access token from $AUTHORIZATION_BASE_URL for user ${token.userAccountId} on client ${token.acHost.key}")

    WS.url(AUTHORIZATION_BASE_URL + AUTHORIZATION_PATH)
      // because we need to sign again in those cases.
      .withFollowRedirects(follow = false)
      .withHeaders("Content-Type" -> "application/x-www-form-urlencoded")
      .withBody(accessTokenRequestBody)
      .withMethod("POST")
  }

  private def accessTokenRequestBody(implicit token: ActingAsUserToken) = {
    val now: Long = System.currentTimeMillis / 1000
    val jwt = jwtWriterFactory.macSigningWriter(SigningAlgorithm.HS256, token.acHost.sharedSecret).jsonToJwt(Json.obj(
      "iss" -> s"urn:atlassian:connect:clientid:${token.acHost.oauthClientId}",
      "sub" -> s"urn:atlassian:connect:useraccountid:${token.userAccountId}",
      "tnt" -> token.acHost.baseUrl,
      "aud" -> AUTHORIZATION_BASE_URL,
      "iat" -> now,
      "exp" -> (now + 10)
    ).toString())

    Map(
      //    "scope" -> Seq(""),
      "assertion" -> Seq(jwt),
      "grant_type" -> Seq(JWT_BEARER_URN)
    )
  }

  private def checkResponse(response: WSResponse): WSResponse = {
    val remaining = response.header("X-RateLimit-Remaining").fold(-1)(_.toInt)
    Logger.debug("Remaining calls to authorization server until reset: " + remaining.toString)

    if (response.status != 200) {
      val error = "Could not retrieve bearer token from Atlassian"
      Logger.error(error + response.body)

      if (response.status == 409 && remaining == 0) {
        lazy val now: Long = System.currentTimeMillis / 1000
        throw RateLimitExceeded(error + " , please try again in a few minutes.",
          response.header("X-RateLimit-Reset").fold(5.minutes.toSeconds)(_.toLong - now).seconds.fromNow
        )
      } else {
        throw new Exception(error + " Response status: " + response.status)
      }
    }
    response
  }
}
