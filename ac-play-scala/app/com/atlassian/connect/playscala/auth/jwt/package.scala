package com.atlassian.connect.playscala.auth

import com.atlassian.jwt.Jwt
import play.api.mvc.Result

import scalaz.\/

package object jwt {
  type JwtAuthenticationResult = Result \/ Jwt
}
