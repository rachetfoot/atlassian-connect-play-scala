package com.atlassian.connect.playscala.auth.jwt

import com.atlassian.connect.playscala.Constants
import play.api.libs.ws.{WS, WSRequestHolder, WSResponse}
import com.atlassian.connect.playscala.auth.{ActingAsUserToken, Token}

import scala.concurrent.Future
import play.api.http.{ContentTypeOf, Writeable}
import Constants._
import play.api.Logger

import scala.concurrent.ExecutionContext.Implicits.global

trait JwtSignedAcHostWS extends JwtConfig with AcBearerTokenGenerator {

  class SignedRequestHolder(val reqHolder: WSRequestHolder) {
    def signedGet()(implicit token: Token, jwtAuthorizationGenerator: JwtAuthorizationGenerator): Future[WSResponse] = {
      signed.get()
    }

    def signedPost[T](body: T)(implicit wrt: Writeable[T], ct: ContentTypeOf[T], token: Token, jwtAuthorizationGenerator: JwtAuthorizationGenerator): Future[WSResponse] = {
      signed.post(body)(wrt, ct)
    }

    def signedPut[T](body: T)(implicit wrt: Writeable[T], ct: ContentTypeOf[T], token: Token, jwtAuthorizationGenerator: JwtAuthorizationGenerator): Future[WSResponse] = {
      signed.put(body)(wrt, ct)
    }

    def signedDelete()(implicit token: Token, jwtAuthorizationGenerator: JwtAuthorizationGenerator): Future[WSResponse] = {
      signed.delete()
    }

    def signedHead()(implicit token: Token, jwtAuthorizationGenerator: JwtAuthorizationGenerator): Future[WSResponse] = {
      signed.head()
    }

    def signedOptions()(implicit token: Token, jwtAuthorizationGenerator: JwtAuthorizationGenerator): Future[WSResponse] = {
      signed.options()
    }

    def signed(implicit token: Token, jwtAuthorizationGenerator: JwtAuthorizationGenerator): WSRequestHolder = {
      val req = token.userAccountId.map(accountId => reqHolder.withQueryString(AC_ACCOUNT_ID_PARAM -> accountId))
        .getOrElse(reqHolder)
        // because we need to sign again in those cases.
        .withFollowRedirects(follow = false)
      req.sign(new JwtSignatureCalculator(jwtAuthorizationGenerator, token.acHost, token.userAccountId, req.queryString))
    }

    def signedAsUserGet(fallback: Boolean = false)(implicit token: Token, jwtAuthorizationGenerator: JwtAuthorizationGenerator): Future[WSResponse] = {
      signedAsUser(fallback) flatMap { req => req.get() }
    }

    def signedAsUserPost[T](body: T, fallback: Boolean = false)(implicit wrt: Writeable[T], ct: ContentTypeOf[T], token: Token, jwtAuthorizationGenerator: JwtAuthorizationGenerator): Future[WSResponse] = {
      signedAsUser(fallback) flatMap { req => req.post(body)(wrt, ct) }
    }

    def signedAsUserPut[T](body: T, fallback: Boolean = false)(implicit wrt: Writeable[T], ct: ContentTypeOf[T], token: Token, jwtAuthorizationGenerator: JwtAuthorizationGenerator): Future[WSResponse] = {
      signedAsUser(fallback) flatMap { req => req.put(body)(wrt, ct) }
    }

    def signedAsUserDelete(fallback: Boolean = false)(implicit token: Token, jwtAuthorizationGenerator: JwtAuthorizationGenerator): Future[WSResponse] = {
      signedAsUser(fallback) flatMap { req => req.delete() }
    }

    def signedAsUserHead(fallback: Boolean = false)(implicit token: Token, jwtAuthorizationGenerator: JwtAuthorizationGenerator): Future[WSResponse] = {
      signedAsUser(fallback) flatMap { req => req.head() }
    }

    def signedAsUserOptions(fallback: Boolean = false)(implicit token: Token, jwtAuthorizationGenerator: JwtAuthorizationGenerator): Future[WSResponse] = {
      signedAsUser(fallback) flatMap { req => req.options() }
    }

    def signedAsUser(fallback: Boolean = false)(implicit token: Token, jwtAuthorizationGenerator: JwtAuthorizationGenerator): Future[WSRequestHolder] = {
      token.asActingAsUserToken map { implicit actingAsUserToken: ActingAsUserToken =>
        getAccessTokenForUser map { bearerToken =>
          reqHolder.withHeaders("Authorization" -> s"Bearer ${bearerToken.value}")
            // because we need to sign again in those cases:
            .withFollowRedirects(follow = false)
        } recover {
          case e: Exception if fallback =>
            Logger.error("Could not retrieve ACT_AS_USER bearer token, falling back to acting as addon user. Caused by: " + e.getMessage)
            signed
        }
      } getOrElse {
        Logger.debug("Do not have oauthId and user context, falling back to acting as addon user")
        Future.successful(signed)
      }
    }

  }

  import scala.language.implicitConversions
  implicit def toSignedRequestHolder(reqHolder: WSRequestHolder) = new SignedRequestHolder(reqHolder)

}
