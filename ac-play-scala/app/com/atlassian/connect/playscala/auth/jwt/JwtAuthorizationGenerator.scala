package com.atlassian.connect.playscala.auth.jwt

import com.atlassian.connect.playscala.AcConfig
import com.atlassian.jwt.writer.{ JwtJsonBuilder, JwtWriter, JwtWriterFactory }
import java.net.{ URI, URLDecoder }
import org.apache.http.util.CharArrayBuffer
import org.apache.http.message.{ BasicHeaderValueParser, ParserCursor }
import org.apache.http.NameValuePair
import org.apache.commons.lang3.StringUtils
import scala.collection.mutable
import com.atlassian.jwt.SigningAlgorithm
import com.atlassian.jwt.core.writer.{ JwtClaimsBuilder, JsonSmartJwtJsonBuilder }
import com.atlassian.jwt.core.TimeUtil
import com.atlassian.jwt.httpclient.{ CanonicalRequestUtil, CanonicalHttpUriRequest }
import org.jboss.netty.handler.codec.http.HttpMethod
import play.Logger
import scala.collection.Map
import scala.collection.JavaConverters._
import com.atlassian.jwt.JwtConstants.HttpRequests._
import com.atlassian.connect.playscala.model.AcHostModel

class JwtAuthorizationGenerator(val jwtWriterFactory: JwtWriterFactory, val jwtExpiryWindowSeconds: Int)(implicit val acConfig: AcConfig) {
  private final val QUERY_DELIMITERS = Array[Char]('&')

  def generate(httpMethodStr: String, url: String, parameters: Map[String, Seq[String]], acHost: AcHostModel, accountId: Option[String]): Option[String] = {
    val method: HttpMethod = HttpMethod.valueOf(httpMethodStr)
    val uri: URI = new URI(url)
    val path: String = uri.getPath
    val baseUrl: URI = new URI(acHost.baseUrl)
    val productContext: String = baseUrl.getPath
    val pathWithoutProductContext: String = path.substring(productContext.length)
    Logger.trace("Creating Jwt signature for:")
    Logger.trace(s"httpMethod: '$httpMethodStr'")
    Logger.trace(s"URL: '$url'")
    Logger.trace(s"acHost key: '${acHost.key}'")
    Logger.trace(s"accountId: '$accountId'")
    Logger.trace(s"Parameters: $parameters")
    Logger.trace(s"pathWithoutProductContext: '$pathWithoutProductContext'")
    val uriWithoutProductContext: URI = new URI(uri.getScheme, uri.getUserInfo, uri.getHost, uri.getPort, pathWithoutProductContext, uri.getQuery, uri.getFragment)
    Logger.trace(s"uriWithoutProductContext: '$uriWithoutProductContext'")
    generate(method, uriWithoutProductContext, parameters, acHost, accountId)
  }

  def generate(httpMethod: HttpMethod, url: URI, parameters: Map[String, Seq[String]], acHost: AcHostModel, accountId: Option[String]): Option[String] = {
    require(parameters != null, "Parameters Map argument cannot be null")
    require(acHost != null)

    val paramsAsArrays: Map[String, Array[String]] = parameters.map {
      case (k, v) => (k, v.toArray)
    }

    Option(JWT_AUTH_HEADER_PREFIX + encodeJwt(httpMethod, url, paramsAsArrays, accountId, acHost))
  }

  private def encodeJwt(httpMethod: HttpMethod, targetPath: URI, params: Map[String, Array[String]], accountIdValue: Option[String], acHost: AcHostModel): String = {
    require(null != httpMethod, "HttpMethod argument cannot be null")
    require(null != targetPath, "URI argument cannot be null")

    var jsonBuilder: JwtJsonBuilder = new JsonSmartJwtJsonBuilder().issuedAt(TimeUtil.currentTimeSeconds).expirationTime(TimeUtil.currentTimePlusNSeconds(jwtExpiryWindowSeconds)).issuer(acConfig.pluginKey)
    accountIdValue.foreach(u => jsonBuilder = jsonBuilder.subject(u))

    var completeParams = mutable.Map.empty[String, Array[String]]
    completeParams ++= params
    if (!StringUtils.isEmpty(targetPath.getQuery)) {
      completeParams ++= constructParameterMap(targetPath)
    }
    val canonicalHttpUriRequest = new CanonicalHttpUriRequest(httpMethod.toString, targetPath.getPath, "", completeParams.asJava)
    Logger.debug("Canonical request is: " + CanonicalRequestUtil.toVerboseString(canonicalHttpUriRequest))
    JwtClaimsBuilder.appendHttpRequestClaims(jsonBuilder, canonicalHttpUriRequest)

    issueJwt(jsonBuilder.build, acHost)
  }

  private def issueJwt(jsonPayload: String, acHost: AcHostModel): String = getJwtWriter(acHost).jsonToJwt(jsonPayload)

  private def getJwtWriter(acHost: AcHostModel): JwtWriter = jwtWriterFactory.macSigningWriter(SigningAlgorithm.HS256, acHost.sharedSecret)

  private def constructParameterMap(uri: URI): Map[String, Array[String]] = {
    val query: String = uri.getQuery
    if (query == null) {
      return Map.empty
    }
    val queryParams = mutable.Map[String, mutable.ArrayBuffer[String]]().withDefault(_ => mutable.ArrayBuffer())
    val buffer: CharArrayBuffer = new CharArrayBuffer(query.length)
    buffer.append(query)
    val cursor: ParserCursor = new ParserCursor(0, buffer.length)
    while (!cursor.atEnd) {
      val nameValuePair: NameValuePair = BasicHeaderValueParser.DEFAULT.parseNameValuePair(buffer, cursor, QUERY_DELIMITERS)
      if (!StringUtils.isEmpty(nameValuePair.getName)) {
        val decodedName: String = urlDecode(nameValuePair.getName)
        val decodedValue: String = urlDecode(nameValuePair.getValue)
        queryParams(decodedName) += decodedValue
      }
    }
    queryParams.mapValues(_.toArray)
  }

  private def urlDecode(content: String): String = if (null == content) null else URLDecoder.decode(content, "UTF-8")
}
