package com.atlassian.connect.playscala.controllers

import com.atlassian.connect.playscala.AcConfigured
import com.atlassian.connect.playscala.model.ClientKey
import play.api.mvc.{ Result, RequestHeader }
import play.api.mvc.Results._
import com.atlassian.connect.playscala.auth.{ CompressedToken, Token }
import play.api.libs.Crypto
import play.api.libs.json.{ JsSuccess, Json, JsValue }
import org.apache.commons.codec.binary.Base64
import scala.concurrent.Future
import scala.util.control.Exception._
import scalaz._, Scalaz._
import play.api.Logger
import com.atlassian.connect.playscala.auth.Token._
import com.atlassian.connect.playscala.store.DbConfiguration

trait PageTokenValidator extends DbConfiguration with AcConfigured {

  def PageTokenValidated(f: Token => Result)(implicit request: RequestHeader): Result =
    PageTokenValidated(allowInsecurePolling = false)(f)

  def PageTokenValidated(allowInsecurePolling: Boolean)(f: Token => Result)(implicit request: RequestHeader): Result =
    PageTokenValidatedGeneric(allowInsecurePolling)(f) valueOr identity

  def PageTokenValidatedAsync(f: Token => Future[Result])(implicit request: RequestHeader): Future[Result] =
    PageTokenValidatedAsync(allowInsecurePolling = false)(f)

  def PageTokenValidatedAsync(allowInsecurePolling: Boolean)(f: Token => Future[Result])(implicit request: RequestHeader): Future[Result] =
    PageTokenValidatedGeneric(allowInsecurePolling)(f) valueOr Future.successful

  def PageTokenValidatedGeneric[A](allowInsecurePolling: Boolean)(f: Token => A)(implicit request: RequestHeader): Result \/ A = {
    getPageTokenText(request) \/> Unauthorized("Unauthorised: It appears your session has expired. Please reload the page.") flatMap { encryptedToken =>
      val maybeToken: Option[Token] = catching(classOf[Throwable]) opt {
        Logger.debug(s"About to decrypt $encryptedToken")
        //Crypto throws Exceptions when there's issues decrypting.  That's normal usage in
        //case someone is trying to fake a token, so lets ignore it here return a None
        Crypto.decryptAES(encryptedToken)
      } flatMap { decrypted =>
        Logger.debug(s"Successfully decrypted $encryptedToken to $decrypted")
        val jsNode: JsValue = Json.parse(new String(Base64.decodeBase64(decrypted.getBytes)))
        Logger.debug(s"Parsed to Json $jsNode")
        Json.fromJson[CompressedToken](jsNode) match {
          case JsSuccess(CompressedToken(hostClientKey, maybeAccountId, maybeP, timestamp), _) =>
            Logger.debug(s"Successfully extract token properties from Json to $hostClientKey $maybeAccountId $maybeP, $timestamp")
            if (hasTimeLeft(timestamp) && (maybeP.isEmpty || allowInsecurePolling)) {
              Logger.debug(s"About to look up by host consumer key $hostClientKey")
              acHostModelStore.findByKey(ClientKey(hostClientKey)).map { acHost =>
                Logger.debug(s"Found AC host $acHost")
                Token(acHost, maybeAccountId, allowInsecurePolling)
              }
            } else {
              Logger.debug("Token expired or requires insecure polling")
              None
            }
          case _ =>
            Logger.debug(s"Failed to extract token properties from Json")
            None
        }
      }
      maybeToken.map(f) \/> Unauthorized("Page token failed validation.")
    }
  }

  private def hasTimeLeft(timestamp: Long): Boolean =
    acConfig.tokenExpiryLimit.toMillis + timestamp > System.currentTimeMillis()

  private def getPageTokenText(req: RequestHeader): Option[String] = {
    req.headers.get(PageTokenHeader).orElse(req.queryString.get(PageTokenParam).flatMap(_.headOption))
  }

  implicit class WithTokenHeader(val result: Result) {
    def withTokenHeader(implicit token: Token): Result = result.withHeaders(PageTokenHeader -> token.encryptedToken)
  }

}
