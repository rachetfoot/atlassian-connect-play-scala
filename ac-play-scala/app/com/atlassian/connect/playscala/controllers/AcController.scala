package com.atlassian.connect.playscala.controllers

import com.atlassian.connect.playscala.{ AcConfigured, PlayAcConfigured }
import play.api.mvc._
import play.api.Logger
import controllers.Assets
import com.atlassian.connect.playscala.model.{ ClientKey, AcHostModel }
import com.atlassian.connect.playscala.util.{ AcHostWS, DescriptorUtils }
import play.api.http.MimeTypes
import play.api.mvc.Accepting
import com.atlassian.connect.playscala.store.DbConfiguration
import scala.concurrent.Future
import play.api.libs.ws.WSResponse
import com.atlassian.connect.playscala.auth.jwt.JwtConfig
import java.net.URI
import scalaz.\/

trait AcController extends Controller with ActionJwtValidator with JwtConfig with DbConfiguration with AcConfigured {

  private val AcceptsApplicationJson = Accepting(MimeTypes.JSON)

  private val defaultDescriptor: RequestHeader => String = _ => DescriptorUtils.substituteVariablesInDefaultFile

  private val defaultHome: RequestHeader => Result = _ => Ok(views.html.ac.internal.index_doc())

  def acIndex(descriptor: => RequestHeader => String = defaultDescriptor, home: => RequestHeader => Result = defaultHome) =
    Action { implicit request =>
      Logger.debug(s"Accepts: ${request.acceptedTypes}")
      Logger.debug(s"Accepts application/json? ${request.accepts(MimeTypes.JSON)}")
      request match {
        case FromUpm() => Ok(descriptor(request)).as(MimeTypes.JSON)
        case Accepts.Html() => home(request)
        case AcceptsApplicationJson() => Ok(descriptor(request)).as(MimeTypes.JSON)
        case _ => BadRequest("Invalid accept header.")
      }
    }

  def acDescriptor() = Action { request =>
    Ok(defaultDescriptor(request)).as(MimeTypes.JSON)
  }

  def acIndexBuilder() = IndexActionBuilder(defaultDescriptor, defaultHome)

  case class IndexActionBuilder(descriptor: RequestHeader => String, home: RequestHeader => Result) {
    def build(): Action[AnyContent] = acIndex(descriptor, home)

    def descriptor(block: => RequestHeader => String): IndexActionBuilder = copy(descriptor = block)

    def home(block: => RequestHeader => Result): IndexActionBuilder = copy(home = block)
  }

  case object FromUpm {
    def unapply(request: RequestHeader): Boolean =
      request.headers.get("X-Pac-Client-Info") exists { header =>
        val isUpm = header.startsWith("client=upm")
        if (isUpm) Logger.debug(s"Upm is requesting the plugin descriptor: $header")
        isUpm
      }
  }

  def registration = Action { implicit req =>
    req.body.asJson flatMap { remoteAppJson =>
      for {
        addonKey <- (remoteAppJson \ "key").asOpt[String]
        clientKey <- (remoteAppJson \ "clientKey").asOpt[String] map ClientKey
        baseUrl <- (remoteAppJson \ "baseUrl").asOpt[String]
        publicKey <- (remoteAppJson \ "publicKey").asOpt[String]
        productType <- (remoteAppJson \ "productType").asOpt[String]
        sharedSecret <- (remoteAppJson \ "sharedSecret").asOpt[String]
      } yield {
        val description = (remoteAppJson \ "description").asOpt[String]
        val oauthClientId = (remoteAppJson \ "oauthClientId").asOpt[String]
        val acHostExisting: Option[AcHostModel] = acHostModelStore.findByKey(clientKey)

        val jwtCheckResult: Result \/ Boolean = acHostExisting map { _: AcHostModel =>
          jwtValidatedGeneric { token => token.acHost.key.value.equals(clientKey.value) } // Prevent vulnerability
        } getOrElse \/.right(true)

        jwtCheckResult.swap valueOr { checkResult =>
          if (checkResult) {
            val acHost: AcHostModel = acHostExisting.fold(
              AcHostModel(id = None, key = clientKey, baseUrl = baseUrl, publicKey = publicKey, productType = productType, description = description, sharedSecret = sharedSecret, oauthClientId = oauthClientId)
            ) {
                host: AcHostModel => host.copy(key = clientKey, baseUrl = baseUrl, publicKey = publicKey, productType = productType, description = description, sharedSecret = sharedSecret, oauthClientId = oauthClientId)
              }

            acHostModelStore.save(acHost)
            Logger.info(s"Successfully registered installation for host $acHost")
            Ok("")
          } else {
            BadRequest("")
          }
        }
      }
    } getOrElse BadRequest("")
  }

  def asset(path: String, file: String) = Assets.at(path, file)

  private def fromJson(req: Request[AnyContent]): Option[AcHostModel] =
    req.body.asJson flatMap { remoteAppJson =>
      for {
        key <- (remoteAppJson \ "key").asOpt[String]
        clientKey <- (remoteAppJson \ "clientKey").asOpt[String] map ClientKey
        baseUrl <- (remoteAppJson \ "baseUrl").asOpt[String]
        publicKey <- (remoteAppJson \ "publicKey").asOpt[String]
        productType <- (remoteAppJson \ "productType").asOpt[String]
        sharedSecret <- (remoteAppJson \ "sharedSecret").asOpt[String]
      } yield {
        val description = (remoteAppJson \ "description").asOpt[String]
        val oauthClientId = (remoteAppJson \ "oauthClientId").asOpt[String]
        AcHostModel(id = None, key = clientKey, baseUrl = baseUrl, publicKey = publicKey, productType = productType, description = description, sharedSecret = sharedSecret, oauthClientId = oauthClientId)
      }
    }

  private val CONSUMER_INFO_URI: URI = URI.create("/plugins/servlet/oauth/consumer-info")

  import scala.concurrent.ExecutionContext.Implicits.global

  def fetchPublicKeyFromRemoteHost(acHost: AcHostModel): Future[String] = {
    val response: Future[WSResponse] = AcHostWS.uri(acHost, CONSUMER_INFO_URI).get()
    response map { resp =>
      if (resp.status == play.api.http.Status.OK) {
        val nodes = resp.xml \\ "publicKey"
        nodes.head.text
      }
      throw new Exception("Failed to fetch public key for verification. Response status: " + resp.status) // yuck
    }
  }

  def registerHost(acHost: AcHostModel): Future[Unit] = {
    fetchPublicKeyFromRemoteHost(acHost) map { fetchedPublicKey =>
      if (fetchedPublicKey != acHost.publicKey) { // TODO strip strings
        throw new Exception("Mismatched keys!")
      }
      acHostModelStore.save(acHost)
    }
  }

}

import com.atlassian.connect.playscala.store.DefaultDbConfiguration

object AcController extends AcController with DefaultDbConfiguration with PlayAcConfigured
