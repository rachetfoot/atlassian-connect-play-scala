package com.atlassian.connect.playscala.controllers

import com.atlassian.connect.playscala.AC
import play.api.Play
import play.api.mvc.{ Request, Result }
import play.api.mvc.Results._
import scala.concurrent.{ ExecutionContext, Future }

trait DevAction {

  protected object DevAction extends ActionWrapper {

    def run[A](delegate: Request[A] => Future[Result])(implicit ec: ExecutionContext): Request[A] => Future[Result] = {
      request => if (AC.isDev) delegate(request) else Future.successful(notFound(request))
    }

    private def notFound[A](request: Request[A]) =
      NotFound(_root_.views.html.defaultpages.notFound(request, Play.maybeApplication.flatMap(_.routes)))
  }

}
