package com.atlassian.connect.playscala

import scala.concurrent.duration.FiniteDuration

trait AcConfigured {
  implicit def acConfig: AcConfig
  @deprecated(message = "Use acConfig.", since = "0.3.1")
  def AC: AcConfig = acConfig
}

trait AcConfig {

  def isDev: Boolean

  def pluginKey: String

  def pluginName: String

  def pluginVersion: Option[String]

  def baseUrl: String

  def tokenExpiryLimit: FiniteDuration

}
