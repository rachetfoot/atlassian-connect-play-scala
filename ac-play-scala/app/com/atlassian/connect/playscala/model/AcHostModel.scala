package com.atlassian.connect.playscala.model

case class ClientKey(value: String)

case class AcHostModel(id: Option[Long], key: ClientKey, publicKey: String,
    baseUrl: String, productType: String, description: Option[String], sharedSecret: String,
    oauthClientId: Option[String]) {

  def asAcHostWithOauthClientId: Option[AcHostWithOauthClientId] = oauthClientId.map(
    AcHostWithOauthClientId(id, key, publicKey, baseUrl, productType, description, sharedSecret, _)
  )
}

case class AcHostWithOauthClientId(id: Option[Long], key: ClientKey, publicKey: String,
  baseUrl: String, productType: String, description: Option[String], sharedSecret: String,
  oauthClientId: String)

trait AcHostModelStore {

  def all(): Seq[AcHostModel]
  def findByKey(clientKey: ClientKey): Option[AcHostModel]
  def findByUrl(baseUrl: String): Option[AcHostModel]
  def save(acHost: AcHostModel): AcHostModel
}
