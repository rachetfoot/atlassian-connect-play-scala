package controllers

import play.api.mvc._
import com.atlassian.connect.play.scala.controllers._

object Application extends AcController with ActionOAuthValidator {

  def index() = acIndex(descriptor = {
    _ => views.xml.descriptor("0.1-SNAPSHOT", com.atlassian.connect.play.scala.AC.baseUrl)
  })

  def browseIssue(oauthConsumerKey: String, issueId: String) = Action { implicit request =>
    OAuthValidated { token =>
      Ok("Everything is ok.")
    }
  }
}